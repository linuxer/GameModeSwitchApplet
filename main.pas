unit main;

{$mode objfpc}{$H+}
{$warnings off}
{$hints off}
//////////////////////////////////////////////////////////////////////////////////////
// Copyright © 2021 linuxer <linuxer@artixlinux.org> <https://linuxer.gr>
// Created at 8th of April 2021 by Linuxer <linuxer@arixlinux.org>, from scratch,
// cause is a new applet, with Free Pascal, based on my coding style, and usage
// of my previous project's
// Licence FPC modified LGPL Version 2
// Thanks to Ido Kanner idokan at@at gmail dot.dot com, for the libnotify FPC library
//////////////////////////////////////////////////////////////////////////////////////

interface

uses
  {$IFDEF UNIX}
          cthreads,
  {$ENDIF}
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, Menus,
  PopupNotifier, Unix, process, UTF8Process, libnotify, about;

type

TNotifier = class

   private
       {$IFDEF LINUX}
               LNotifier : PNotifyNotification;
       {$ELSE}
              LocalTimer : TFPTimer;
              PopupNotifier: TPopupNotifier;
              procedure TimerFinished( Sender : TObject );
       {$ENDIF}
   public
       procedure ShowTheMessage(const Title, Message : string; IconIndex : String; ShowTime : integer);
       destructor Destroy; Override;
       constructor Create();
end;

type

  { TFormMain }

  TFormMain = class(TForm)
    ImageListModes     : TImageList;
    MenuItem1          : TMenuItem;
    MenuItem2          : TMenuItem;
    MenuItem3          : TMenuItem;
    MenuItem4          : TMenuItem;
    MenuItem5          : TMenuItem;
    MenuItem6          : TMenuItem;
    MenuItem7          : TMenuItem;
    PopupMenu1         : TPopupMenu;
    TrayIcon1          : TTrayIcon;
    procedure FormCreate(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure MenuItem3Click(Sender: TObject);
    procedure MenuItem5Click(Sender: TObject);
    procedure TrayIcon1Click(Sender: TObject);
  private

  public

  end;

var
  FormMain                 : TFormMain;
  S                     : LongInt;
  n                     : Integer;
  DaemonStatus          : Boolean;

implementation

{$R *.frm}

{ TFormMain }

Procedure TNotifier.ShowTheMessage(const Title, Message : string; IconIndex : String; ShowTime : integer);
begin
     {$IFDEF LINUX}
             notify_init(argv[0]);
             LNotifier := notify_notification_new (pchar(Title), pchar(Message), pchar(IconIndex)); //'dialog-warning'));
             notify_notification_set_timeout(LNotifier, ShowTime);                // figure is mS
             notify_notification_show (LNotifier, nil);
             notify_uninit;
             Destroy;
             // Should also check for errors and use TPopupNotifier if Notify won't work
             // But that will have to wait until I find a Linux where it does not work .....
     {$ELSE}
            // For the future to be further develop to set x,y position ----> POSSIBLE TODO ON NEXT RELEASES
            // Non Linux must use TPopupNotifier
            PopupNotifier := TPopupNotifier.Create(nil);
            PopupNotifier.Text  := Message;
            PopupNotifier.Title := Title;
            PopupNotifier.show;
            LocalTimer := TFPTimer.create(nil);
            LocalTimer.Interval := ShowTime;
            LocalTimer.OnTimer:= @TimerFinished;
            LocalTimer.Enabled := True;
     {$ENDIF}
end;

{$IFNDEF LINUX}
         procedure TNotifier.TimerFinished( Sender : TObject );
         begin
              //  writeln('Timer finished');
              LocalTimer.Enabled := false;
              PopupNotifier.hide;
              Destroy;
         end;
{$ENDIF}

destructor TNotifier.Destroy;
begin
    {$IFNDEF LINUX}
             freeandnil(PopupNotifier);
             freeandnil(LocalTimer);
    {$ENDIF}
    inherited Destroy;
end;

constructor TNotifier.Create();
begin
    inherited Create();
end;

procedure GetGameModeDaemonStatus;
var
     AStringList : TStringList;
     AProcess    : TProcess;
     PIDStr      : String;
begin
     DaemonStatus           := false;
     AProcess               := TProcess.Create(nil);
     AProcess.Executable    := '/bin/sh';
     AProcess.Parameters.Add('-c');
     AProcess.Parameters.Add('pidof gamemoded');
     AProcess.Options       := AProcess.Options + [poWaitOnExit, poUsePipes];
     AProcess.Execute;
     AStringList            := TStringList.Create;
     AStringList.LoadFromStream(AProcess.Output);
     PIDStr                 := (AStringList.Text);
     if Length(PIDStr) > 0 then
        begin
             DaemonStatus := true;
        end;
     AStringList.Free;
     AProcess.Free;
end;

procedure GameModeOn;
begin
    if (DaemonStatus = false) then
       begin
           S := fpsystem('gamemoded&');
           DaemonStatus := true;
       end;
end;

procedure GameModeOff;
begin
    if (DaemonStatus = true) then
       begin
           //ShowMessage('Incorrect password');
           S := fpsystem('pkill gamemoded');
           DaemonStatus := false;
       end;
end;

procedure PopUpGameModedDaemonOn;
var
   PopNotifier  : TNotifier;

begin
     {$IFNDEF LINUX}
              FormMain.PopupNotifier2.Text  := 'Game Mode Daemon Running';
              FormMain.PopupNotifier2.ShowAtPos(NewWidth , NewHeight - 325 div 2 - 20);
     {$ENDIF}
     {$IFDEF LINUX}
             PopNotifier := TNotifier.Create;
             PopNotifier.ShowTheMessage('Game Mode Daemon Running', '', 'info', 3000);
     {$ENDIF}
end;

procedure PopUpGameModedDaemonOff;
var
   PopNotifier  : TNotifier;

begin
     {$IFNDEF LINUX}
              FormMain.PopupNotifier2.Text  := 'Game Mode Daemon Not Running';
              FormMain.PopupNotifier2.ShowAtPos(NewWidth , NewHeight - 325 div 2 - 20);
     {$ENDIF}
     {$IFDEF LINUX}
             PopNotifier := TNotifier.Create;
             PopNotifier.ShowTheMessage('Game Mode Daemon Not Running', '', 'info', 3000);
     {$ENDIF}
end;

procedure TFormMain.FormCreate(Sender: TObject);
begin
    application.showmainform    := false;
    GetGameModeDaemonStatus;
    if (DaemonStatus = true) then
       begin
            ImageListModes.GetIcon(0, TrayIcon1.Icon);
            TrayIcon1.Hint      := 'Game Mode Daemon Running';
            PopUpGameModedDaemonOn;
            MenuItem1.Enabled   := false;
            MenuItem2.Enabled   := true;
            //ShowMessage('Game Mode Daemon Running');
       end
    else
       begin
            ImageListModes.GetIcon(1, TrayIcon1.Icon);
            TrayIcon1.Hint      := 'Game Mode Daemon Not Running';
            PopUpGameModedDaemonOff;
            MenuItem1.Enabled   := true;
            MenuItem2.Enabled   := false;
            //ShowMessage('Game Mode Daemon Not Running');
       end;
    TrayIcon1.ShowIcon          := True;
    TrayIcon1.Show;
    //ImageListModes.GetIcon(0, TrayIcon1.Icon);
end;

procedure TFormMain.MenuItem1Click(Sender: TObject);
begin
     GameModeOn;
     ImageListModes.GetIcon(0, TrayIcon1.Icon);
     TrayIcon1.Hint      := 'Game Mode Daemon Running';
     MenuItem1.Enabled   := false;
     MenuItem2.Enabled   := true;
     PopUpGameModedDaemonOn;
end;

procedure TFormMain.MenuItem2Click(Sender: TObject);
begin
     GameModeOff;
     ImageListModes.GetIcon(1, TrayIcon1.Icon);
     TrayIcon1.Hint      := 'Game Mode Daemon Not Running';
     MenuItem1.Enabled   := true;
     MenuItem2.Enabled   := false;
     PopUpGameModedDaemonOff;
end;

procedure TFormMain.MenuItem3Click(Sender: TObject);
begin
    TrayIcon1.Hide;
    PopupMenu1.Free;
    TrayIcon1.Free;
    Halt (0);
end;

procedure TFormMain.MenuItem5Click(Sender: TObject);
begin
     FormAbout := TFormAbout.Create(Self);
     try
        FormAbout.Label1.Caption:='Project';
        FormAbout.Label2.Caption:='Developer';
        FormAbout.Label3.Caption:='Licence';
        FormAbout.ListBox1.Items.Clear;
        FormAbout.ListBox1.Items.Add('Game Mode Swaith Applet for Artix Linux is an');
        FormAbout.ListBox1.Items.Add('LGPL2 Free Pascal Project with LCL components,');
        FormAbout.ListBox1.Items.Add('completely developed from scratch, at 8th, of');
        FormAbout.ListBox1.Items.Add('April 2021, © by linuxer パスカリス スポシト');
        FormAbout.ShowModal;
     finally
        FormAbout := nil;
        //FormAbout.Free;
        //FreeAndNil(formAbout)
     end;
    //FormAbout.Show;
end;

procedure TFormMain.TrayIcon1Click(Sender: TObject);
begin
    PopupMenu1.PopUp;
end;

end.
