unit about;

{$mode objfpc}{$H+}

//////////////////////////////////////////////////////////////////////////////////////
// Copyright © 2021 linuxer <linuxer@artixlinux.org> <https://linuxer.gr>
// Created at 8th of April 2021 by Linuxer <linuxer@arixlinux.org>, as a part of a
// applet, with Free Pascal, based on my coding style, and usage of my previous
// project's code
// Licence FPC modified LGPL Version 2
// Thanks to Ido Kanner idokan at@at gmail dot.dot com, for the libnotify FPC library
//////////////////////////////////////////////////////////////////////////////////////

interface

uses
  {$IFDEF UNIX}
          cthreads,
  {$ENDIF}
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  TplLabelUnit;

type

  { TFormAbout }

  TFormAbout = class(TForm)
    ButtonClose: TButton;
    Image1: TImage;
    Image2: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    ListBox1: TListBox;
    plURLLabel1: TplURLLabel;
    plURLLabel2: TplURLLabel;
    plURLLabel3: TplURLLabel;
    procedure ButtonCloseClick(Sender: TObject);
    procedure Image1Click(Sender: TObject);

  private

  public

  end;

var
  FormAbout: TFormAbout;

implementation

{$R *.frm}

{ TFormAbout }


procedure TFormAbout.ButtonCloseClick(Sender: TObject);
begin
   //FormAbout.Close;
   ModalResult:=mrCancel
end;

procedure TFormAbout.Image1Click(Sender: TObject);
begin

end;

end.
